# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "A lighttpd container image"

require app-container-image.bb
require app-container-image-lighttpd-common.inc
